import pickle
from app import app
from re import search
from datetime import datetime
from db import create_connection
from update_time import update_time
from flask import render_template, redirect, request


sort_songs = None
column_choise = {'artist': 1, 'song': 2, 'duration': 3, 'id': 0}


def write_time():
    now = datetime.now()
    with open('data_test', 'wb') as time_file:
        pickle.dump(now, time_file)


def sorting(songs):
    if sort_songs is not None:
        column = column_choise[sort_songs]
        songs = sorted(songs, key=lambda x: x[column])
    return songs


def test_adding(artist, song, duration):
    """For testing that all filds filled correctly"""
    if artist and song and duration:
        test_duration = search(r"\d{2}:\d{2}:\d{2}", duration)
        if test_duration.group() is not None:
            test_duration = duration.replace(test_duration.group(), '')
            if not test_duration:
                return True


@app.route('/')
def index():
    conn = create_connection('db.sqlite3')
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM music_list")
    songs = sorting(cursor.fetchall())
    number_of_songs = len(songs)
    last_update = update_time()
    context = {
        'songs': songs
    }
    return render_template(
                            'index.html', number=number_of_songs,
                            last_update=last_update, **context
                            )


@app.route('/add_song/', methods=['POST'])
def add_song():
    test = test_adding(
                request.form['author'], request.form['song'],
                request.form['time']
                )
    if test:
        author = request.form['author'].replace('\'', ' ')
        song = request.form['song'].replace('\'', ' ')
        conn = create_connection('db.sqlite3')
        cursor = conn.cursor()
        cursor.execute(
            f"INSERT INTO music_list (author, song, time)"
            f" VALUES ('{author}',"
            f" '{song}', '{request.form['time']}')",
            )
        conn.commit()
        write_time()
    return redirect('/')


@app.route('/delete_song/', methods=['POST'])
def delete_song():
    conn = create_connection('db.sqlite3')
    cursor = conn.cursor()
    cursor.execute(
        f"DELETE FROM music_list WHERE id={request.form['id']}"
        )
    conn.commit()
    write_time()
    return redirect('/')


@app.route('/sorting_songs/', methods=['POST'])
def sorting_songs():
    global sort_songs
    sort_songs = list(request.form.values())[0]
    return redirect('/')
