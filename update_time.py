import pickle
from datetime import datetime


def word_ending(time_):
    if time_ == 1:
        letter = ''
    else:
        letter = 's'

    return letter


def exact_time(seconds):
    if seconds <= 59:
        letter = word_ending(seconds)
        difference = f"{seconds} second{letter}"
    elif seconds <= 3599:
        minutes = seconds // 60
        letter = word_ending(minutes)
        difference = f"{minutes} minute{letter}"
    else:
        hours = seconds // 3600
        letter = word_ending(hours)
        difference = f"{hours} hour{letter}"

    return difference


def time_or_date(restored_data):
    new_now = datetime.now()
    difference = new_now - restored_data
    difference_str = str(difference)[:-7]
    if len(difference_str) > 8:
        difference = difference_str.split(',')[0]
    else:
        seconds = difference.seconds
        difference = exact_time(seconds)

    return difference


def update_time():
    with open('data_test', 'rb') as time_file:
        restored_data = pickle.load(time_file)
    if restored_data:
        last_update = time_or_date(restored_data)
    else:
        last_update = '0 seconds'

    return last_update
